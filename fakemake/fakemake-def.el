;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'org-clhs
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("org-clhs-core"
                       "org-clhs")
  org-files-for-testing-in-order '("org-clhs-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
